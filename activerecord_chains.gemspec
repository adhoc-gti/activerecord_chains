Gem::Specification.new do |s|
    s.name        = 'activerecord_chains'
    s.version     = '0.0.0'
    s.licenses    = ['BSD-3-Clause']
    s.summary     = 'Access rules evaluation inspired by iptables'
    s.description = <<-DESC
      Chains enable definition and evaluation of advanced rules of access to
      persisted ActiveRecord instances at the record or scope level.
    DESC
    s.authors     = ['ADHOC-GTI']
    s.email       = 'foss@adhoc-gti.com'
    s.files       = Dir['lib/**/*.rb'] + ['LICENSE']
    s.homepage    = 'https://github.com/adhoc-gti/activerecord_chains'
  end